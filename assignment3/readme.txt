LAB 4:
READ ME FILE:

Upload the Jar file created into Hadoop cluster and execute following commands:-

Question 1)

Input file: Q1.java
Output file:opt1.txt

Command:
hadoop jar yelp.jar assignment3.assignment3.Q1  /yelpdatafall/business/business.csv  /user/pgr150030/opt1
-------------------
Question 2)

Input file: Q2.java
Output file:opt2.txt

Command:
hadoop jar yelp.jar assignment3.assignment3.Q2  /yelpdatafall/business/business.csv  /user/pgr150030/opt2
-------------------
Question 3)

Input file: TopN.java
Output file:opt3.txt

Command:
hadoop jar yelp.jar assignment3.assignment3.TopN  /yelpdatafall/business/business.csv  /user/pgr150030/opt3
-------------------
Question 4)

Input file: Q4.java
Output file:opt4.txt

Command:
hadoop jar yelp.jar assignment3.assignment3.Q4  /user/pgr150030/file1.txt /user/pgr150030/opt4
-------------------
Question 5)

Input file: Q5.java
Output file:opt5.txt

Command:
hadoop jar yelp.jar assignment3.assignment3.Q5  /yelpdatafall/review/review.csv  /user/pgr150030/opt5
-------------------
Question 6)

Input file: Q6.java
Output file:opt6.txt

Command:
hadoop jar yelp.jar assignment3.assignment3.Q6  /yelpdatafall/review/review.csv  /user/pgr150030/opt6
 

